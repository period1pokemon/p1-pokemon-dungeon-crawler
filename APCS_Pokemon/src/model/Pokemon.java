package model;

import java.util.ArrayList;

//Class that keeps track of the attributes of the Pokemon. 

//name is the name of the Pokemon
//pokemonID is the ID of the Pokemon from bulbapedia that is used in the hashTable
//experience is the progress towards leveling up and gaining stats
//health is the current health of the Pokemon
//maxHealth is the maximum health of a Pokemon
public class Pokemon{

	public String name;
	public int pokemonID;

	public int level;
	public int experience; //0-100
	public int health;
	public int maxHealth;
	public int attack;

	// ArrayList for moves (moves are stored as integers)
	public ArrayList<PokemonMove> moves = new ArrayList<PokemonMove>(); //ArrayList will contain the IDs of moves

	// Constructor
	public Pokemon(String name, int ID, int maxHealth){
		this.name = name;
		this.pokemonID = ID;
		this.level = 1;
		this.experience = 0;
		this.maxHealth = maxHealth;
		this.health = this.maxHealth;
	}

	public Pokemon(String name, int ID, int maxHealth, int attack){
		this.name = name;
		this.pokemonID = ID;
		this.level = 1;
		this.experience = 0;
		this.maxHealth = maxHealth;
		this.health = this.maxHealth;
		this.attack = attack;
	}

	public Pokemon(String name, int ID, int level, int experience, int maxHealth, int attack){
		this.name = name;
		this.pokemonID = ID;
		this.level = level;
		this.experience = experience;
		this.maxHealth = maxHealth;
		this.health = this.maxHealth;
		this.attack = attack;
	}

	// Adds a move to the Pokemon's moveset
	public void learnMove(PokemonMove move){
		moves.add(move);
	}

	// The Pokemon gains experience. If exp exceeds 100, then the Pokemon levels up
	// and the exp bar resets
	public boolean gainExperience(int experience){
		boolean levelup = false;
		this.experience += experience;
		
		if(this.experience >= 100){
			level++;
			this.experience -= 100;
			// increase stat on level up
			this.attack += 2;
			this.maxHealth += 2;
			levelup = true;

			resetHP();
		}

		return levelup;
	}

	public void resetHP(){
		health = maxHealth;
	}

	// Pokemon loses health
	public void loseHealth(int healthLost){
		this.health -= healthLost;
	}

	public String toString(){
		return this.name;
	}

}
