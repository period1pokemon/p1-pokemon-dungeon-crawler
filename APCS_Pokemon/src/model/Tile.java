package model;

// Class to store the image name and boolean values of a tile. This is stored
// in an array in the model class. This makes it so the model class can have only
// one array that contains Tile objects instead of multiple arrays with different
// attributes. 
public class Tile {

	String imageName;
	boolean walkable;
	boolean pokemonCanAppear;
	
	public Tile(String name, boolean walk){
		imageName = name;
		walkable = walk;

		if(imageName.equals("shrub.png")){
			pokemonCanAppear = true;
		}else{
			pokemonCanAppear = false;
		}
	}
	
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public boolean isWalkable() {
		return walkable;
	}

	public void setWalkable(boolean walkable) {
		this.walkable = walkable;
	}

	public boolean pokemonCanAppear(){
		return pokemonCanAppear;
	}

}
