package model;

public class NPCmap {
	//2d array for map with NPC objects where they should be
	public NPC[][] charactersOnMap;
	
	//get array list of npcs to put into the map for NPCs 2d array
	public NPCmap(int size, NPC[] npcs){
		charactersOnMap = new NPC[size][size];
		for(int i = 0; i < npcs.length; i++){
			int col = npcs[i].location.x;
			int row = npcs[i].location.y;
			charactersOnMap[col][row] = npcs[i];
		}
	}
}
