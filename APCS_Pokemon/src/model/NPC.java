package model;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import view.PokemonDialogueView;

public class NPC extends KeyAdapter{
	int map; //the map the character is on
	String name; // Name of the non player character (ex. Professor Oak)
	Point location; // the point on the map that the NPC is at
					// (col, row)
	int direction; // The direction the NPC is facing
	Image[] sprites = new Image[4]; // sprites for the NPC,
														// like for facing
														// different directions
	ArrayList<String> dialogs = new ArrayList<String>();
	PokemonDialogueView dialogue;
	int dialogueChoice = 0; 
	public boolean isTalking = false;
	//direction: 0: North 1: East 2: South 3: West
	public NPC(int col, int row, int dir, int map){
		location = new Point(col, row);
		direction = dir;
		this.map = map;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER){
			
			if(isTalking){
				disposeDialogue();
				isTalking = false;
			}
						
		}
	}
		
	
	//shows the string using view's jframe given the index the string is at in the NPC's
	//dialog choices
	public void chooseDialogToShow(int index, JFrame frame){
		String msgToDisplay = dialogs.get(index);
		JOptionPane dialog = new JOptionPane();
		dialog.showMessageDialog(frame, msgToDisplay, name, JOptionPane.INFORMATION_MESSAGE, null);	
	}
	
	public void addDialog(String message){
		dialogs.add(message);
	}
	
	public boolean dialogueOptionSpoken(int index){
		if(dialogueChoice > index) return true;
		return false;
	}
	
	//shows current dialogue choice
	public void showDialogue(int xPos, int yPos){
		String msgToDisplay = dialogs.get(dialogueChoice);
		dialogue = new PokemonDialogueView(msgToDisplay, name, xPos, yPos);
		dialogue.addKeyListener(this);
	}
	
	public void disposeDialogue(){
		dialogue.dispose();
	}
	
	//advances the dialogue shown
	public void advanceDialogue(){
		dialogueChoice++;
	}
	
	//returns whether or not the character is standing next to and facing this NPC
	//to be called if the player is on the map the character is on
	public boolean charIsFacing(int dir, int col, int row, int offsetY, int offsetX){
		if((int)(location.x - offsetX) == col && (int)(location.y - offsetY) == row - 1 && dir == 0) return true;
		else if(location.x - offsetX == col + 1 && location.y - offsetY == row && dir == 1) return true;
		else if(location.x - offsetX == col && location.y - offsetY == row + 1 && dir == 2) return true;
		else if(location.x - offsetX == col - 1 && location.y - offsetY == row && dir == 3) return true;
		return false;
	}
	
	public void setSprite(Image img, int spriteDir){
		sprites[spriteDir] = img;
	}
	
	public Image getImage(){
		return sprites[direction];
	}
	
	public Point getLocation(){
		return location;
	}
	
	public String getName(){
		return name;
	}
	
	public int getMap(){
		return map;
	}
	
}